/*
 * Copyright 2019 (c) Martin Owens <doctormo@gmail.com>
 *
 * License: AGPLv3 (see LICENCE.txt for details)
 *
 */

/* Game configuration */
var game_file = 'data/game.yaml';
var game = null; // not loaded yet
var current_event = null;
var limit_movement = null;
var signal_button = false;

/* Game state variables */
var bar_min_height = 27.67536;
var bar_max_height = 254.28059;
var bar_min_top = 350.41742;
var bar_max_top = 124.18009;
var fade_speed = 300;

/*
 * Returns true if the location is accessable from the currently location
 */
function is_accessable(from, loc) {
    var movs = game.movements[from];
    if(!movs) { movs = [] }
    if(limit_movement && limit_movement.indexOf(loc) == -1) {
        return false;
    }
    return !current_event && (movs.indexOf(loc) != -1 || (game.movements.all.indexOf(loc) != -1 && loc != from));
}

document.addEventListener("DOMContentLoaded", function(event) {
    // WARNING: Do not put anything except signals in this function, see load_game()
    var pcs = document.querySelectorAll('#pcs use');
    for(var index in pcs) {
        var shape = SVG.get(pcs[index].id);
        if(shape) {
            shape.attr('style', null);
            shape.attr('class', 'absent');
        }
    }

    var locs = document.querySelectorAll('#locations *');
    for(var index in locs) {
        var elem = document.getElementById(locs[index].id);
        var shape = SVG.get(locs[index].id);
        if(elem && elem.addEventListener) {
            shape.attr('location', true);
            shape.attr('style', null);
            shape.attr('class', 'out');
            elem.addEventListener("click", function() {
                if(game && is_accessable(game.state.location, this.id)) {
                    change_location(this.id);
                }
            });
            elem.addEventListener("mouseout", function() {
                this.setAttribute('class', 'out');
            });
            elem.addEventListener("mousemove", function() {
                if(game && is_accessable(game.state.location, this.id)) {
                    this.setAttribute('class', 'in');
                }
            });
        }
    }

    document.getElementById('button_a').addEventListener("click", function() { event_next('a'); })
    document.getElementById('button_b').addEventListener("click", function() { event_next('b'); })
    document.getElementById('button_c').addEventListener("click", function() { event_next('c'); })
    document.addEventListener("click", function() { event_next(''); });

    new_game(game_file);
});

/*
 * Load a YAML file and callback the data when it's ready.
 */
function load_yaml(file, callback, arg1) {
    var request = new XMLHttpRequest();
    request.open("GET", file);
    request.responseType = 'text';
    request.send();

    request.onreadystatechange = function() {
        if (request.readyState == 4) {
            if (request.status == 200) {
                callback(jsyaml.load(request.responseText), arg1);
            } else {
                alert("Can't load game file: " + file);
            }
        }
    }
}
/*
 * Loads a new state file and parses it, passes it to init_game()
 */
function new_game(game_file) {
    load_yaml(game_file, function(game) {
        var ready = 0;
        // Locations are in their own yaml, load any chained
        // yaml locations and only when ready, call init game.
        for(var key in game.locations) {
            load_yaml(game.locations[key], function(data, key) {
                //console.log(key, game.locations[key]);
                game.locations[key] = data;
                ready--;
                if(ready == 0) { init_game(game) }
            }, key);
            ready++;
        }
        if(ready == 0) { init_game(game) }
    });
}

/*
 * Data from the gama_data is loaded into the game creating signals.
 */
function init_game(data) {
    game = data;

    // First location is in the game yaml
    change_location(game.state.location);
}

/*
 * Called when update state.
 */
function update_state() {
    for(key in game.bars) {
        var value = game.state[key];
        var bar = SVG.get('bar_'+key);
        var pc = (value - game.bars[key].min) / game.bars[key].max;

        var height = ((bar_max_height - bar_min_height) * pc) + bar_min_height;
        var y = ((bar_max_top - bar_min_top) * pc) + bar_min_top;

        bar.animate(1000, '>').height(height).y(y);
    }
}

/*
 * Fade in the right avatar (or remove avatar if null)
 */
function red_says(key, text, btn1, btn2, btn3) {
    var npc = game.npc[key];
    var green_bubble = SVG.get('green_bubble');
    var red_bubble = SVG.get('red_bubble');
    var holder = SVG.get('red_avatar_holder');
    var avatar = SVG.get('red_avatar');

    var talker = holder.attr('talker');

    if(holder.attr('opacity') != 0) {
        if (talker != key || key == null) {
            // Fade out the existing talker and fade in a new one.
            holder.attr('talker', null);
            red_bubble.animate(fade_speed).attr('opacity', 0);
            holder.animate(fade_speed).attr('opacity', 0).afterAll(function() {
                red_says(key, text, btn1, btn2, btn3);
            });
            return;
        }
    } else if(key != null) {
        // Fade in the new talker
        holder.visible(true);
        holder.attr('talker', key);
        avatar.attr('xlink:href', npc.avatar);
        holder.animate(fade_speed).attr('opacity', 1).afterAll(function() {
            red_says(key, text, btn1, btn2, btn3);
        });
        var name = document.querySelector('#red_title h3');
        name.textContent = npc.name;
        return;
    }

    if(text) {
        var div = document.querySelector('#red_bubble div.inner');
        // TODO: Animate this (somehow)
        div.textContent = text;
        red_bubble.animate(fade_speed).attr('opacity', 1).afterAll(said);
        green_bubble.animate(fade_speed).attr('opacity', 0);
        set_button('a', btn1);
        set_button('b', btn2);
        set_button('c', btn3);
        signal_button = btn1 || btn2 || btn3;
    } else {
        red_bubble.animate(fade_speed).attr('opacity', 0);
    }
}
/*
 * Set the specific button with the given text and show it.
 */
function set_button(key, text) {
    var elem = document.getElementById('button_' + key);
    //console.log("button", elem, text);
    if(text) {
        elem.textContent = text;
        elem.style.visibility = 'visible';
    } else {
        elem.style.visibility = 'hidden';
    }
}
/*
 * Your own player dialog, replacing red bubble text.
 */
function green_says(text) {
    var green_bubble = SVG.get('green_bubble');
    var red_bubble = SVG.get('red_bubble');
    var red_holder = SVG.get('red_avatar_holder');

    if(red_bubble.attr('opacity') != 0) {
        red_says(red_holder.attr('talker'), null);
    }
    var div = document.querySelector('#green_bubble div.inner');
    if(text) {
        div.textContent = text;
        green_bubble.animate(fade_speed).attr('opacity', 1).afterAll(said);
    } else {
        div.textContent = '';
        green_bubble.animate(fade_speed).attr('opacity', 0);
    }
    signal_button = false;
}
/*
 * Clear all bubbles (usually after world tick)
 */
function noone_says() {
    var green_bubble = SVG.get('green_bubble');
    var red_bubble = SVG.get('red_bubble');
    var red_holder = SVG.get('red_avatar_holder');

    green_bubble.attr('opacity', 0);
    red_bubble.attr('opacity', 0);
    red_holder.attr('opacity', 0);
}
/*
 * After popping up a bubble, we want to re-enable the
 * signals that can go to the next dialog option.
 */
function said() {
    if(current_event) {
        current_event.display = true;
    }
}

/*
 * Each day causes one tick of the world clock.
 */
function world_clock() {
    if(game.state.finished) {
        var layer = SVG.get('end_' + game.state.finished);
        layer.attr('style', '');
        return;
    }
    // Reset dialog visuals
    noone_says();

    // Progress clock
    game.state.clock++;

    // Calculate work
    var work = game.state.work * 2;
    work = work + game.state.star_help;
    work = work + game.state.hermit_help / 2;
    work = work + game.state.boss_help / 3;
    work = work + game.state.fool_help / 4;

    // Calculate cost to business
    game.state.business = game.state.business - game.state.cost + work;

    // Calculate promotion chances
    game.state.promotion = game.state.star_help + game.state.hermit_help
        + game.state.boss_help + game.state.fool_help;

    update_state();
    console.log("Tick", game.state);
    if(!current_event) {
        event_get(game.state.location);
    }
}

/*
 * Change the location and add a tick to the world clock
 */
function change_location(loc, frozen) {
    limit_movement = null;
    game.state.location = loc;
    var pc = document.getElementById('pc_at_' + loc);
    var old_pc = document.querySelector('.visiting');
    if(pc) {
        if(old_pc) {
            old_pc.setAttribute('class', 'absent');
        }
        pc.setAttribute('class', 'visiting');
    }
    if(!frozen) {
        world_clock();
    }
}

/*
 * Get an event from the stack of events for the given location
 */
function event_get(loc) {
    // Events that could happen anywhere
    var events = game.locations.all;
    for(var key in events) {
        var ev = events[key];
        if(event_check(key, ev)) {
            if(ev.repeat !== true) {
                ev.repeat--;
            }
            event_start(ev);
            return;
        }
    }

    // Location specific events
    var events = game.locations[loc];
    for(var key in events) {
        var ev = events[key];
        if(event_check(key, ev)) {
            if(ev.repeat != true) {
                ev.repeat--;
            }
            event_start(ev);
            return;
        }
    }
}

/*
 * Starts a dialog chain (loads a new one)
 */
function event_start(ev) {
    current_event = {
        'ev': ev,
        'dialog': ev.dialog,
        'index': -1,
        'choices': '',
        'display': true,
    }
    if(ev.move_to) {
        change_location(ev.move_to, true);
    }
    event_next('');
}

/*
 * Advance the dialog to the next item
 */
function event_next(choice) {
    if(current_event && !current_event.display) {
        console.log("Not ready!");
        return;
    }
    if(!choice && signal_button) {
        console.log("Press a button!");
        return;
    }
    if(current_event) {
        //console.log("event_next", choice, current_event);
        current_event.choices = current_event.choices + choice;
        var dialog = get_next_event();
        //console.log(" - dialog", dialog);
        if(dialog) {
            current_event.index = dialog.index;
            current_event.display = false;
            if(dialog.branch) {
                // Re-sync branches in case the branches got pruned.
                current_event.choices = dialog.branch;
            }
            var has_next = get_next_event()

            if(dialog.npc) {
                red_says(dialog.npc, dialog.text, dialog.option_a, dialog.option_b, dialog.option_c);
            } else {
                green_says(dialog.text);
            }
            if(dialog.then) {
                apply_states(dialog.then);
            }
            if(!has_next || dialog.end) {
                // There's no possible next dialog
                if(current_event.ev.then) {
                    apply_states(current_event.ev.then);
                }
                if(current_event.ev.movement) {
                    limit_movement = current_event.ev.movement;
                    for(var index in limit_movement) {
                        SVG.get(limit_movement[index]).attr('class', 'inout');
                    }
                }
                current_event = null;
                if(game.state.finished) {
                    world_clock();
                }
            }
        } else {
            console.error("Dialog chain just stopped...");
            current_event = null;
        }
    }
}

/*
 * What would be the next dialog in the current event.
 */
function get_next_event(offset) {
    if(offset == undefined) { offset = 1; }
    current_event.choices
    var dialog = current_event.dialog[current_event.index + offset];
    if(dialog) {
        if(dialog.index == undefined) {
            dialog.index = current_event.index + offset;
        }
        if(dialog.branch == undefined) {
            dialog.branch = '';
        }
        if(dialog.branch.startsWith(current_event.choices) || dialog.branch == "*") {
            return dialog;
        }
        return get_next_event(offset + 1);
    }
    return null;
}

/*
 * Apply a list of states
 */
function apply_states(states) {
    for(var key in states) {
        var stmod = states[key];
        var current = game.state[key];
        if(typeof current == "number" && typeof stmod == "string") {
            var op = stmod.substr(0, 1);
            var num = parseInt(stmod.substr(1));
            if(op == "+") {
                game.state[key] = current + num;
            } else if(op == "-") {
                game.state[key] = current - num;
            } else if(op == "*") {
                game.state[key] = current * num;
            } else if(op == "/") {
                game.state[key] = current / num;
            } else {
                console.error("Can't apply state:", key, op, num);
            }
        } else {
            // Blind setter
            game.state[key] = stmod;
        }
    }
}

/*
 * Check an event's conditionals to see if we should do this event.
 */
function event_check(ev_name, ev) {
    var repeat_check = true;
    var chance_check = true;
    var state_check = true;
    if(ev.repeat != undefined) { repeat_check = ev.repeat >= 0; }
    if(ev.chance != undefined) { chance_check = Math.random() <= ev.chance; }
    if(ev.state != undefined) {
        for(var key in ev.state) {
            var stcheck = ev.state[key];
            var current = game.state[key];
            if(current != undefined) {
                if(typeof current == "number" && typeof stcheck == "string") {
                    // Special comparison
                    var cmp = stcheck.substr(0, 1);
                    var num = parseInt(stcheck.substr(1));
                    if(cmp == ">") {
                        state_check = state_check && current > num;
                    } else if(cmp == "<") {
                        state_check = state_check && current < num;
                    } else if(cmp == "=") {
                        state_check = state_check && current == num;
                    }
                } else {
                    // String equality check
                    state_check = state_check && stcheck == current;
                }
            }
        }
    }
    //console.log(ev_name, repeat_check, chance_check, state_check);
    return repeat_check && chance_check && state_check;
}
