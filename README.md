The idea is an office based strategy game. You start the game being hired by a company, the company contains four other people. Your task is to gain recognition for your work (these are win points).

You are the "only one" who does any real work for the company, you are the only one who is a female employee, you must run around not just taking care of your own work, but helping your boss, and colleagues do theirs while fending off micro aggression and pure randomness of the world.

There are three lose conditions.

 1. The company fails (because no one does any work)
 1. You get fired for not doing your work
 1. you get "promoted" into one of the caring roles for the rest of the company.

The game is built using SVG with the text blocks and buttons being xhtml foreignObject additions. The SVG is styled via CSS, while a game engine has been written from scratch for this game which loads in yaml files for both game state and dialog trees to make it all flexible.

Up to date project is available on GitLab: https://gitlab.com/doctormo/one-works-game/tree/master Don't forget to contribute if you're interested. This is an open source game (AGPLv3).

Big thanks to Walter Sickert and his art, which is used in the avatars and finishing images.
